-------------------------------------------
--------Fire Alarm System - README --------
-------------------------------------------

The fire alarm system scenario is used for a small building in which three rooms exist: a factory, an office and a floor, which is devided into 2 sub-floors a and b.
The building looks like depicted in the following figure:
                                 _________
								|         |
								| office  |
   _____________________________|_________|
  |                                       |
  |    floor_a      |     floor_b         |
  |_______________________________________|
  |                 |
  |                 |
  |    factory      |
  |                 |
  |_________________|

In these rooms it is possible, that a fire starts to burn anywhere. Thereby the fire has 4 states:

	- no fire: nothing burns in a room
	- smoldering fire: only a small fire, in which light smoke occurs - until 15min after fire starts burning
	- flashover: smoke starts burning because of oxygen - hot - until 18min after fire starts bruning
	- full fire: flames, dense smoke, very hot  - starting after 19min after fire starts burning
	
Moreover it should be possible that the fire is able to broaden, whereby any room starts burning if its neighbour room burns 10min.
This means between the following rooms such an interaction may happen:

	- factory and floor_a
	- floor_a and floor_b
	- floor_b and office
	
The owner of the building installed a fire alarm system to protect the rooms.
With respect to this, every room has only a smoke-detector except the factory which has an additional heat detector.
These detectors will trigger an alarm signal to a central controller , if they detect a fire. 
Concerning the factory both detectors have to detect a fire to trigger a fire signal.
This is necessary, because normal smoke caused by the working process could lead to wrong alarms.
The detectors detect a fire with different probabilities concerning the different fire-state of the room, in which they are installed.

The central controller use a triggered alarm caused at least one detector to do the following actions:

	- trigger alarm to all detectors
	- trigger signal to the two emgergency fire doors to close
	- trigger alarm to emergency call center (call resuce forces)

The two emergency fire doors are located between the two floor parts floor_a and floor_b and between floor_b and the office. 
It is important to know, that each fire door can block the broadening of the fire.
All parts of the system are extended with a failure model, so that every part may fail. 
Exceptions are the triggering of the information from the detectors to the central controller. 
This could be added to the model.

The model consists of many components, which are explained in the following 

-> complete_system: - decscripes wrapper for the system

-> rooms_environment: - descripes wrapper for the environment especially rooms and fire states and broadening

-> fire_alarm_system: - descripes wrapper for the fire alarm system

component rooms_environment consists of:

-components, which describe the burning time of a room:
	- burn_time_factory
	- burn_time_floor_a
	- burn_time_floor_b
	- burn_time_office
	
-components, which describe the fire state in a room	
	- fire_state_factory
	- fire_state_floor_a
	- fire_state_floor_b
	- fire_state_office

-components, which describe wheater the fire started in a room or not
	- start_fire_factory
	- start_fire_floor_a
	- start_fire_floor_b
	- start_fire_office

component fire_alarm_system consists of:

-components, which describe the behaviour of the central controller:
	- failures
		- controller_alarm2detectors_fails
		- controller_alarm2doors_fails
		- controller_alarm2emergency_fails
		- controller_fails
	- normal behaviour
		- fire_alarm_controller_detectors
		- fire_alarm_controller_doors
		- fire_alarm_controller_emergency
	
- components, which describe the behaviour of the emergency fire doors
	- failures
		- emergency_fire_door_floor2office_fails
		- emergency_fire_door_floors_fails
	- normal behaviour
		- emergency_fire_door_floor2office
		- emergency_fire_door_floors

- components, which describe the behaviour of the different detectors
	- failures
		- heat_detector_factory_fails_no_alarm_flashover
		- heat_detector_factory_fails_no_alarm_full_fire
		- heat_detector_factory_fails_no_alarm_smoldering
		- heat_detector_factory_fails_wrong_alarm
		- smoke_detector_factory_fails_no_alarm_flashover
		- smoke_detector_factory_fails_no_alarm_full_fire
		- smoke_detector_factory_fails_no_alarm_smoldering
		- smoke_detector_factory_fails_wrong_alarm.smoke_detector_factory_fails_wrong_alarm
		- smoke_detector_factory_fails_no_alarm_flashover
		- smoke_detector_factory_fails_no_alarm_full_fire
		- smoke_detector_factory_fails_no_alarm_smoldering
		- smoke_detector_factory_fails_wrong_alarm.smoke_detector_factory_fails_wrong_alarm
		- smoke_detector_floor_a_fails_no_alarm_flashover
		- smoke_detector_floor_a_fails_no_alarm_full_fire
		- smoke_detector_floor_a_fails_no_alarm_smoldering
		- smoke_detector_floor_a_fails_wrong_alarm.smoke_detector_floor_a_fails_wrong_alarm
		- smoke_detector_floor_b_fails_no_alarm_flashover
		- smoke_detector_floor_b_fails_no_alarm_full_fire
		- smoke_detector_floor_b_fails_no_alarm_smoldering
		- smoke_detector_floor_b_fails_wrong_alarm
		- smoke_detector_office_fails_no_alarm_flashover
		- smoke_detector_office_fails_no_alarm_full_fire
		- smoke_detector_office_fails_no_alarm_smoldering
		- smoke_detector_office_fails_wrong_alarm
	- normal behaviour	
		- heat_detector_factory
		- smoke_detector_factory
		- smoke_detector_floor_a
		- smoke_detector_floor_b
		- smoke_detector_office
