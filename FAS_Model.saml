component complete_system	
	
	/************* constants ************/
	
	// fire-states in a room
	enum FIRE_STATE := [NO_FIRE, SMOLDERING_FIRE, FLASHOVER, FULL_FIRE];
	enum DOOR_STATE := [OPEN, CLOSE];
	enum BURNS_STATE := [NO,YES];
	enum ALARM_STATE := [OFF,ON];
	enum SIGNAL_STATE := [UNSET, SET];
	
	component rooms_environment
	
		/********* local constants *********/
		constant int max_time := 19;
	
		/***********************************/
	
		/*********** formulas  *************/
	
		// set room on fire
		formula burns_factory := start_fire_factory.set_burns = BURNS_STATE.YES | broadending_floor_a;
		formula burns_floor_a := start_fire_floor_a.set_burns = BURNS_STATE.YES | broadending_factory |
								  (broadending_floor_b & !(fire_alarm_system.emergency_fire_door_floors.state=DOOR_STATE.CLOSE));
		formula burns_floor_b := start_fire_floor_b.set_burns = BURNS_STATE.YES | 
								 (broadending_floor_a & !(fire_alarm_system.emergency_fire_door_floors.state=DOOR_STATE.CLOSE)) | 
								 (broadending_office & !(fire_alarm_system.emergency_fire_door_floor2office.state=DOOR_STATE.CLOSE));
		formula burns_office := start_fire_office.set_burns = BURNS_STATE.YES | 
								(broadending_floor_b & !(fire_alarm_system.emergency_fire_door_floor2office.state=DOOR_STATE.CLOSE));
		
		// broadening fire
		formula broadending_factory := burn_time_factory.burn_time >= 10;
		formula broadending_floor_a := burn_time_floor_a.burn_time >= 5;
		formula broadending_floor_b := burn_time_floor_a.burn_time >= 5;
		formula broadending_office := burn_time_floor_a.burn_time >= 10;
		
		/***********************************/
	
		/******************************************************************************/
		/*fire in single rooms*/	
	
		component start_fire_factory
			set_burns : BURNS_STATE init BURNS_STATE.NO;
			
			set_burns = BURNS_STATE.NO -> choice:(1:(set_burns'=BURNS_STATE.NO)) + 
					  		  choice:(1:(set_burns'=BURNS_STATE.YES) );
			set_burns = BURNS_STATE.YES -> choice:(1:(set_burns'=BURNS_STATE.YES));
		endcomponent
			
		component burn_time_factory
			
			formula stopCounting := burn_time=max_time;
			
			// minutes while burning
			// 19 denotes 19min or longer
			burn_time : [0..max_time] init 0;
			
			!burns_factory 				  -> choice:(1:(burn_time'=0));
			burns_factory & !stopCounting -> choice:(1:(burn_time'=burn_time+1));
			burns_factory & stopCounting -> choice:(1:(burn_time'=max_time));
			
		endcomponent
			
		component fire_state_factory
			formula startFlashover := burn_time_factory.burn_time >= 15;
			formula startFullFire := burn_time_factory.burn_time >= max_time-1;
	
			// fire states like shown above
			fire_state : FIRE_STATE init FIRE_STATE.NO_FIRE;
			
			!burns_factory -> choice:(1: (fire_state'=FIRE_STATE.NO_FIRE));
			burns_factory & fire_state = FIRE_STATE.NO_FIRE -> choice:(1: (fire_state'=FIRE_STATE.SMOLDERING_FIRE));
			burns_factory & fire_state = FIRE_STATE.SMOLDERING_FIRE & !startFlashover -> choice:(1: (fire_state'=FIRE_STATE.SMOLDERING_FIRE));
			burns_factory & fire_state = FIRE_STATE.SMOLDERING_FIRE & startFlashover -> choice:(1: (fire_state'=FIRE_STATE.FLASHOVER));
			burns_factory & fire_state = FIRE_STATE.FLASHOVER & !startFullFire -> choice:(1: (fire_state'=FIRE_STATE.FLASHOVER));
			burns_factory & fire_state = FIRE_STATE.FLASHOVER & startFullFire -> choice:(1: (fire_state'=FIRE_STATE.FULL_FIRE));
			burns_factory & fire_state = FIRE_STATE.FULL_FIRE -> choice:(1:(fire_state'=FIRE_STATE.FULL_FIRE));
		endcomponent
	
		component start_fire_floor_a
			set_burns : BURNS_STATE init BURNS_STATE.NO;
			
			set_burns = BURNS_STATE.NO -> choice:(1:(set_burns'=BURNS_STATE.NO)) + 
					  		  choice:(1:(set_burns'=BURNS_STATE.YES) );
			set_burns = BURNS_STATE.YES -> choice:(1:(set_burns'=BURNS_STATE.YES));
		endcomponent
			
		component burn_time_floor_a
			formula stopCounting := burn_time=max_time;
			
			// minutes while burning
			// 19 denotes 19min or longer
			burn_time : [0..max_time] init 0;
			
			!burns_floor_a 				  -> choice:(1:(burn_time'=0));
			burns_floor_a & !stopCounting -> choice:(1:(burn_time'=burn_time+1));
			burns_floor_a & stopCounting -> choice:(1:(burn_time'=max_time));
			
		endcomponent
			
		component fire_state_floor_a
			formula startFlashover := burn_time_floor_a.burn_time >= 15;
			formula startFullFire := burn_time_floor_a.burn_time >= max_time-1;
	
			// fire states like shown above
			fire_state : FIRE_STATE init FIRE_STATE.NO_FIRE;
			
			!burns_floor_a -> choice:(1: (fire_state'=FIRE_STATE.NO_FIRE));
			burns_floor_a & fire_state = FIRE_STATE.NO_FIRE -> choice:(1: (fire_state'=FIRE_STATE.SMOLDERING_FIRE));
			burns_floor_a & fire_state = FIRE_STATE.SMOLDERING_FIRE & !startFlashover -> choice:(1: (fire_state'=FIRE_STATE.SMOLDERING_FIRE));
			burns_floor_a & fire_state = FIRE_STATE.SMOLDERING_FIRE & startFlashover -> choice:(1: (fire_state'=FIRE_STATE.FLASHOVER));
			burns_floor_a & fire_state = FIRE_STATE.FLASHOVER & !startFullFire -> choice:(1: (fire_state'=FIRE_STATE.FLASHOVER));
			burns_floor_a & fire_state = FIRE_STATE.FLASHOVER & startFullFire -> choice:(1: (fire_state'=FIRE_STATE.FULL_FIRE));
			burns_floor_a & fire_state = FIRE_STATE.FULL_FIRE -> choice:(1:(fire_state'=FIRE_STATE.FULL_FIRE));
		endcomponent
		
		component start_fire_floor_b
			set_burns : BURNS_STATE init BURNS_STATE.NO;
			
			set_burns = BURNS_STATE.NO -> choice:(1:(set_burns'=BURNS_STATE.NO)) + 
					  		  choice:(1:(set_burns'=BURNS_STATE.YES) );
			set_burns = BURNS_STATE.YES -> choice:(1:(set_burns'=BURNS_STATE.YES));
		endcomponent
			
		component burn_time_floor_b
			formula stopCounting := burn_time=max_time;
			
			// minutes while burning
			// 19 denotes 19min or longer
			burn_time : [0..max_time] init 0;
			
			!burns_floor_b 				  -> choice:(1:(burn_time'=0));
			burns_floor_b & !stopCounting -> choice:(1:(burn_time'=burn_time+1));
			burns_floor_b & stopCounting -> choice:(1:(burn_time'=max_time));
			
		endcomponent
			
		component fire_state_floor_b
			formula startFlashover := burn_time_floor_b.burn_time >= 15;
			formula startFullFire := burn_time_floor_b.burn_time >= max_time-1;
	
			// fire states like shown above
			fire_state : FIRE_STATE init FIRE_STATE.NO_FIRE;
			
			!burns_floor_b -> choice:(1: (fire_state'=FIRE_STATE.NO_FIRE));
			burns_floor_b & fire_state = FIRE_STATE.NO_FIRE -> choice:(1: (fire_state'=FIRE_STATE.SMOLDERING_FIRE));
			burns_floor_b & fire_state = FIRE_STATE.SMOLDERING_FIRE & !startFlashover -> choice:(1:(fire_state'=FIRE_STATE.SMOLDERING_FIRE));
			burns_floor_b & fire_state = FIRE_STATE.SMOLDERING_FIRE & startFlashover -> choice:(1:(fire_state'=FIRE_STATE.FLASHOVER));
			burns_floor_b & fire_state = FIRE_STATE.FLASHOVER & !startFullFire -> choice:(1:(fire_state'=FIRE_STATE.FLASHOVER));
			burns_floor_b & fire_state = FIRE_STATE.FLASHOVER & startFullFire -> choice:(1:(fire_state'=FIRE_STATE.FULL_FIRE));
			burns_floor_b & fire_state = FIRE_STATE.FULL_FIRE -> choice:(1:(fire_state'=FIRE_STATE.FULL_FIRE));
		endcomponent
	
		component start_fire_office
			set_burns : BURNS_STATE init BURNS_STATE.NO;
			
			set_burns = BURNS_STATE.NO -> choice:(1:(set_burns'=BURNS_STATE.NO)) + 
					  		  choice:(1:(set_burns'=BURNS_STATE.YES) );
			set_burns = BURNS_STATE.YES -> choice:(1:(set_burns'=BURNS_STATE.YES));
		endcomponent
			
		component burn_time_office
			formula stopCounting := burn_time=max_time;
			
			// minutes while burning
			// 19 denotes 19min or longer
			burn_time : [0..max_time] init 0;
			
			!burns_office 				  -> choice:(1:(burn_time'=0));
			burns_office & !stopCounting -> choice:(1:(burn_time'=burn_time+1));
			burns_office & stopCounting -> choice:(1:(burn_time'=max_time));
			
		endcomponent
			
		component fire_state_office
			formula startFlashover := burn_time_office.burn_time >= 15;
			formula startFullFire := burn_time_office.burn_time >= max_time-1;
	
			// fire states like shown above
			fire_state : FIRE_STATE init FIRE_STATE.NO_FIRE;
			
			!burns_office -> choice:(1: (fire_state'=FIRE_STATE.NO_FIRE));
			burns_office & fire_state = FIRE_STATE.NO_FIRE -> choice:(1: (fire_state'=FIRE_STATE.SMOLDERING_FIRE));
			burns_office & fire_state = FIRE_STATE.SMOLDERING_FIRE & !startFlashover -> choice:(1: (fire_state'=FIRE_STATE.SMOLDERING_FIRE));
			burns_office & fire_state = FIRE_STATE.SMOLDERING_FIRE & startFlashover -> choice:(1: (fire_state'=FIRE_STATE.FLASHOVER));
			burns_office & fire_state = FIRE_STATE.FLASHOVER & !startFullFire -> choice:(1: (fire_state'=FIRE_STATE.FLASHOVER));
			burns_office & fire_state = FIRE_STATE.FLASHOVER & startFullFire -> choice:(1: (fire_state'=FIRE_STATE.FULL_FIRE));
			burns_office & fire_state = FIRE_STATE.FULL_FIRE -> choice:(1:(fire_state'=FIRE_STATE.FULL_FIRE));
		endcomponent 
		
	endcomponent
	
	// fire alarm system
	component fire_alarm_system 	
	 	/******************************************************************************/
		/* smoke and heat detector */
		
		// probabilities for alarm in different detectors
		constant double p_alarm_heat_NO_FIRE := 5e-03;
		constant double p_alarm_heat_SMOLDERING := 0.3;
		constant double p_alarm_heat_FLASHOVER := 0.999;
		constant double p_alarm_heat_FULL_FIRE := 0.999999;
		
		constant double p_alarm_smoke_NO_FIRE := 0.2;
		constant double p_alarm_smoke_SMOLDERING := 0.9;
		constant double p_alarm_smoke_FLASHOVER := 0.999999;
		constant double p_alarm_smoke_FULL_FIRE := 0.999999999;
		
		/** failure states for detectors **/
		// heat detector factory
		failure heat_detector_factory_fails_wrong_alarm
			occurs perdemand rooms_environment.fire_state_factory.fire_state = FIRE_STATE.NO_FIRE probability  p_alarm_heat_NO_FIRE;
		endfailure
		
		failure heat_detector_factory_fails_no_alarm_smoldering
			occurs perdemand rooms_environment.fire_state_factory.fire_state = FIRE_STATE.SMOLDERING_FIRE probability  1-p_alarm_heat_SMOLDERING;
		endfailure
		
		failure heat_detector_factory_fails_no_alarm_flashover
			occurs perdemand rooms_environment.fire_state_factory.fire_state = FIRE_STATE.FLASHOVER probability  1-p_alarm_heat_FLASHOVER;
		endfailure
		
		failure heat_detector_factory_fails_no_alarm_full_fire
			occurs perdemand rooms_environment.fire_state_factory.fire_state = FIRE_STATE.FULL_FIRE probability  1-p_alarm_heat_FULL_FIRE;
		endfailure
		
		//smoke detector factory
		failure smoke_detector_factory_fails_wrong_alarm
			occurs perdemand rooms_environment.fire_state_factory.fire_state = FIRE_STATE.NO_FIRE probability  p_alarm_smoke_NO_FIRE;
		endfailure
		
		failure smoke_detector_factory_fails_no_alarm_smoldering
			occurs perdemand rooms_environment.fire_state_factory.fire_state = FIRE_STATE.SMOLDERING_FIRE probability  1-p_alarm_smoke_SMOLDERING;
		endfailure
		
		failure smoke_detector_factory_fails_no_alarm_flashover
			occurs perdemand rooms_environment.fire_state_factory.fire_state = FIRE_STATE.FLASHOVER probability  1-p_alarm_smoke_FLASHOVER;
		endfailure
		
		failure smoke_detector_factory_fails_no_alarm_full_fire
			occurs perdemand rooms_environment.fire_state_factory.fire_state = FIRE_STATE.FULL_FIRE probability  1-p_alarm_smoke_FULL_FIRE;
		endfailure
	
		// smoke detector floor_a
		failure smoke_detector_floor_a_fails_wrong_alarm
			occurs perdemand rooms_environment.fire_state_floor_a.fire_state = FIRE_STATE.NO_FIRE probability  p_alarm_smoke_NO_FIRE;
		endfailure
		
		failure smoke_detector_floor_a_fails_no_alarm_smoldering
			occurs perdemand rooms_environment.fire_state_floor_a.fire_state = FIRE_STATE.SMOLDERING_FIRE probability  1-p_alarm_smoke_SMOLDERING;
		endfailure
		
		failure smoke_detector_floor_a_fails_no_alarm_flashover
			occurs perdemand rooms_environment.fire_state_floor_a.fire_state = FIRE_STATE.FLASHOVER probability  1-p_alarm_smoke_FLASHOVER;
		endfailure
		
		failure smoke_detector_floor_a_fails_no_alarm_full_fire
			occurs perdemand rooms_environment.fire_state_floor_a.fire_state = FIRE_STATE.FULL_FIRE probability  1-p_alarm_smoke_FULL_FIRE;
		endfailure
	
		// smoke detector floor_b
		failure smoke_detector_floor_b_fails_wrong_alarm
			occurs perdemand rooms_environment.fire_state_floor_b.fire_state = FIRE_STATE.NO_FIRE probability  p_alarm_smoke_NO_FIRE;
		endfailure
		
		failure smoke_detector_floor_b_fails_no_alarm_smoldering
			occurs perdemand rooms_environment.fire_state_floor_b.fire_state = FIRE_STATE.SMOLDERING_FIRE probability  1-p_alarm_smoke_SMOLDERING;
		endfailure
		
		failure smoke_detector_floor_b_fails_no_alarm_flashover
			occurs perdemand rooms_environment.fire_state_floor_b.fire_state = FIRE_STATE.FLASHOVER probability  1-p_alarm_smoke_FLASHOVER;
		endfailure
		
		failure smoke_detector_floor_b_fails_no_alarm_full_fire
			occurs perdemand rooms_environment.fire_state_floor_b.fire_state = FIRE_STATE.FULL_FIRE probability  1-p_alarm_smoke_FULL_FIRE;
		endfailure
	
		// smoke detector office
		failure smoke_detector_office_fails_wrong_alarm
			occurs perdemand rooms_environment.fire_state_office.fire_state = FIRE_STATE.NO_FIRE probability  p_alarm_smoke_NO_FIRE;
		endfailure
		
		failure smoke_detector_office_fails_no_alarm_smoldering
			occurs perdemand rooms_environment.fire_state_office.fire_state = FIRE_STATE.SMOLDERING_FIRE probability  1-p_alarm_smoke_SMOLDERING;
		endfailure
		
		failure smoke_detector_office_fails_no_alarm_flashover
			occurs perdemand rooms_environment.fire_state_office.fire_state = FIRE_STATE.FLASHOVER probability  1-p_alarm_smoke_FLASHOVER;
		endfailure
		
		failure smoke_detector_office_fails_no_alarm_full_fire
			occurs perdemand rooms_environment.fire_state_office.fire_state = FIRE_STATE.FULL_FIRE probability  1-p_alarm_smoke_FULL_FIRE;
		endfailure
		
		/**************************/
		
		formula anyAlarm := fire_alarm_controller_detectors.signal = SIGNAL_STATE.SET;  		
		
		component heat_detector_factory
			alarm : ALARM_STATE init ALARM_STATE.OFF;

			formula isNoFire := rooms_environment.fire_state_factory.fire_state = FIRE_STATE.NO_FIRE;
			formula isSmoldering := rooms_environment.fire_state_factory.fire_state = FIRE_STATE.SMOLDERING_FIRE;
			formula isFlashover := rooms_environment.fire_state_factory.fire_state = FIRE_STATE.FLASHOVER;
			formula isFullFire := rooms_environment.fire_state_factory.fire_state = FIRE_STATE.FULL_FIRE;
	
			formula failsNoFire := heat_detector_factory_fails_wrong_alarm;
			formula failsSmoldering := heat_detector_factory_fails_no_alarm_flashover;
			formula failsFlashover := heat_detector_factory_fails_no_alarm_flashover;
			formula failsFullFire := heat_detector_factory_fails_no_alarm_full_fire;
	
			alarm = ALARM_STATE.OFF & (isNoFire | isSmoldering | isFlashover | isFullFire) &
			anyAlarm -> choice:(1:(alarm'=ALARM_STATE.ON));
	
			(alarm = ALARM_STATE.OFF & isNoFire) &
			!anyAlarm &  failsNoFire -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isNoFire) &
			!anyAlarm & !failsNoFire -> choice:(1:(alarm'=ALARM_STATE.OFF));
			
			(alarm = ALARM_STATE.OFF & isSmoldering) &
			!anyAlarm & !failsSmoldering -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isSmoldering) &
			!anyAlarm & failsSmoldering -> choice:(1:(alarm'=ALARM_STATE.OFF));
								
			(alarm = ALARM_STATE.OFF & isFlashover) &
			!anyAlarm & !failsFlashover -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isFlashover) &
			!anyAlarm & failsFlashover -> choice:(1:(alarm'=ALARM_STATE.OFF));
							    
			(alarm = ALARM_STATE.OFF & isFullFire) &
			!anyAlarm & !failsFullFire -> choice:(1:(alarm'=ALARM_STATE.ON)); 
			(alarm = ALARM_STATE.OFF & isFullFire) &
			!anyAlarm & failsFullFire -> choice:(1:(alarm'=ALARM_STATE.OFF)); 
			 				    
			alarm = ALARM_STATE.ON -> choice:(1:(alarm'=ALARM_STATE.ON));
		endcomponent
	 	
	 	component smoke_detector_factory
			alarm : ALARM_STATE init ALARM_STATE.OFF;

			formula isNoFire := rooms_environment.fire_state_factory.fire_state = FIRE_STATE.NO_FIRE;
			formula isSmoldering := rooms_environment.fire_state_factory.fire_state = FIRE_STATE.SMOLDERING_FIRE;
			formula isFlashover := rooms_environment.fire_state_factory.fire_state = FIRE_STATE.FLASHOVER;
			formula isFullFire := rooms_environment.fire_state_factory.fire_state = FIRE_STATE.FULL_FIRE;
	
			formula failsNoFire := smoke_detector_factory_fails_wrong_alarm;
			formula failsSmoldering := smoke_detector_factory_fails_no_alarm_flashover;
			formula failsFlashover := smoke_detector_factory_fails_no_alarm_flashover;
			formula failsFullFire := smoke_detector_factory_fails_no_alarm_full_fire;
	
			alarm = ALARM_STATE.OFF & (isNoFire | isSmoldering | isFlashover | isFullFire) &
			anyAlarm -> choice:(1:(alarm'=ALARM_STATE.ON));
	
			(alarm = ALARM_STATE.OFF & isNoFire) &
			!anyAlarm &  failsNoFire -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isNoFire) &
			!anyAlarm & !failsNoFire -> choice:(1:(alarm'=ALARM_STATE.OFF));
			
			(alarm = ALARM_STATE.OFF & isSmoldering) &
			!anyAlarm & !failsSmoldering -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isSmoldering) &
			!anyAlarm & failsSmoldering -> choice:(1:(alarm'=ALARM_STATE.OFF));
								
			(alarm = ALARM_STATE.OFF & isFlashover) &
			!anyAlarm & !failsFlashover -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isFlashover) &
			!anyAlarm & failsFlashover -> choice:(1:(alarm'=ALARM_STATE.OFF));
							    
			(alarm = ALARM_STATE.OFF & isFullFire) &
			!anyAlarm & !failsFullFire -> choice:(1:(alarm'=ALARM_STATE.ON)); 
			(alarm = ALARM_STATE.OFF & isFullFire) &
			!anyAlarm & failsFullFire -> choice:(1:(alarm'=ALARM_STATE.OFF)); 
			 				    
			alarm = ALARM_STATE.ON -> choice:(1:(alarm'=ALARM_STATE.ON));
		endcomponent
	 		
	 	component smoke_detector_floor_a
			alarm : ALARM_STATE init ALARM_STATE.OFF;

			formula isNoFire := rooms_environment.fire_state_floor_a.fire_state = FIRE_STATE.NO_FIRE;
			formula isSmoldering := rooms_environment.fire_state_floor_a.fire_state = FIRE_STATE.SMOLDERING_FIRE;
			formula isFlashover := rooms_environment.fire_state_floor_a.fire_state = FIRE_STATE.FLASHOVER;
			formula isFullFire := rooms_environment.fire_state_floor_a.fire_state = FIRE_STATE.FULL_FIRE;
	
			formula failsNoFire := smoke_detector_floor_a_fails_wrong_alarm;
			formula failsSmoldering := smoke_detector_floor_a_fails_no_alarm_flashover;
			formula failsFlashover := smoke_detector_floor_a_fails_no_alarm_flashover;
			formula failsFullFire := smoke_detector_floor_a_fails_no_alarm_full_fire;
	
			alarm = ALARM_STATE.OFF & (isNoFire | isSmoldering | isFlashover | isFullFire) &
			anyAlarm -> choice:(1:(alarm'=ALARM_STATE.ON));
	
			(alarm = ALARM_STATE.OFF & isNoFire) &
			!anyAlarm &  failsNoFire -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isNoFire) &
			!anyAlarm & !failsNoFire -> choice:(1:(alarm'=ALARM_STATE.OFF));
			
			(alarm = ALARM_STATE.OFF & isSmoldering) &
			!anyAlarm & !failsSmoldering -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isSmoldering) &
			!anyAlarm & failsSmoldering -> choice:(1:(alarm'=ALARM_STATE.OFF));
								
			(alarm = ALARM_STATE.OFF & isFlashover) &
			!anyAlarm & !failsFlashover -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isFlashover) &
			!anyAlarm & failsFlashover -> choice:(1:(alarm'=ALARM_STATE.OFF));
							    
			(alarm = ALARM_STATE.OFF & isFullFire) &
			!anyAlarm & !failsFullFire -> choice:(1:(alarm'=ALARM_STATE.ON)); 
			(alarm = ALARM_STATE.OFF & isFullFire) &
			!anyAlarm & failsFullFire -> choice:(1:(alarm'=ALARM_STATE.OFF)); 
			 				    
			alarm = ALARM_STATE.ON -> choice:(1:(alarm'=ALARM_STATE.ON));
			
		endcomponent
		
		component smoke_detector_floor_b
			alarm : ALARM_STATE init ALARM_STATE.OFF;
	
			formula isNoFire := rooms_environment.fire_state_floor_b.fire_state = FIRE_STATE.NO_FIRE;
			formula isSmoldering := rooms_environment.fire_state_floor_b.fire_state = FIRE_STATE.SMOLDERING_FIRE;
			formula isFlashover := rooms_environment.fire_state_floor_b.fire_state = FIRE_STATE.FLASHOVER;
			formula isFullFire := rooms_environment.fire_state_floor_b.fire_state = FIRE_STATE.FULL_FIRE;
	
			formula failsNoFire := smoke_detector_floor_b_fails_wrong_alarm;
			formula failsSmoldering := smoke_detector_floor_b_fails_no_alarm_flashover;
			formula failsFlashover := smoke_detector_floor_b_fails_no_alarm_flashover;
			formula failsFullFire := smoke_detector_floor_b_fails_no_alarm_full_fire;
	
			alarm = ALARM_STATE.OFF & (isNoFire | isSmoldering | isFlashover | isFullFire) &
			anyAlarm -> choice:(1:(alarm'=ALARM_STATE.ON));
	
			(alarm = ALARM_STATE.OFF & isNoFire) &
			!anyAlarm &  failsNoFire -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isNoFire) &
			!anyAlarm & !failsNoFire -> choice:(1:(alarm'=ALARM_STATE.OFF));
			
			(alarm = ALARM_STATE.OFF & isSmoldering) &
			!anyAlarm & !failsSmoldering -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isSmoldering) &
			!anyAlarm & failsSmoldering -> choice:(1:(alarm'=ALARM_STATE.OFF));
								
			(alarm = ALARM_STATE.OFF & isFlashover) &
			!anyAlarm & !failsFlashover -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isFlashover) &
			!anyAlarm & failsFlashover -> choice:(1:(alarm'=ALARM_STATE.OFF));
							    
			(alarm = ALARM_STATE.OFF & isFullFire) &
			!anyAlarm & !failsFullFire -> choice:(1:(alarm'=ALARM_STATE.ON)); 
			(alarm = ALARM_STATE.OFF & isFullFire) &
			!anyAlarm & failsFullFire -> choice:(1:(alarm'=ALARM_STATE.OFF)); 
			 				    
			alarm = ALARM_STATE.ON -> choice:(1:(alarm'=ALARM_STATE.ON));
		endcomponent
		
		component smoke_detector_office
			alarm : ALARM_STATE init ALARM_STATE.OFF;
			
			formula isNoFire := rooms_environment.fire_state_office.fire_state = FIRE_STATE.NO_FIRE;
			formula isSmoldering := rooms_environment.fire_state_office.fire_state = FIRE_STATE.SMOLDERING_FIRE;
			formula isFlashover := rooms_environment.fire_state_office.fire_state = FIRE_STATE.FLASHOVER;
			formula isFullFire := rooms_environment.fire_state_office.fire_state = FIRE_STATE.FULL_FIRE;
	
			formula failsNoFire := smoke_detector_office_fails_wrong_alarm;
			formula failsSmoldering := smoke_detector_office_fails_no_alarm_flashover;
			formula failsFlashover := smoke_detector_office_fails_no_alarm_flashover;
			formula failsFullFire := smoke_detector_office_fails_no_alarm_full_fire;
	
			alarm = ALARM_STATE.OFF & (isNoFire | isSmoldering | isFlashover | isFullFire) &
			anyAlarm -> choice:(1:(alarm'=ALARM_STATE.ON));
	
			(alarm = ALARM_STATE.OFF & isNoFire) &
			!anyAlarm &  failsNoFire -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isNoFire) &
			!anyAlarm & !failsNoFire -> choice:(1:(alarm'=ALARM_STATE.OFF));
			
			(alarm = ALARM_STATE.OFF & isSmoldering) &
			!anyAlarm & !failsSmoldering -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isSmoldering) &
			!anyAlarm & failsSmoldering -> choice:(1:(alarm'=ALARM_STATE.OFF));
								
			(alarm = ALARM_STATE.OFF & isFlashover) &
			!anyAlarm & !failsFlashover -> choice:(1:(alarm'=ALARM_STATE.ON));
			(alarm = ALARM_STATE.OFF & isFlashover) &
			!anyAlarm & failsFlashover -> choice:(1:(alarm'=ALARM_STATE.OFF));
							    
			(alarm = ALARM_STATE.OFF & isFullFire) &
			!anyAlarm & !failsFullFire -> choice:(1:(alarm'=ALARM_STATE.ON)); 
			(alarm = ALARM_STATE.OFF & isFullFire) &
			!anyAlarm & failsFullFire -> choice:(1:(alarm'=ALARM_STATE.OFF)); 
			 				    
			alarm = ALARM_STATE.ON -> choice:(1:(alarm'=ALARM_STATE.ON));
		endcomponent
		
		/******************************************************************************/
		/* fire alarm controller */
		// triggers alarm to all other smoke and heat detectors
		// triggers signal to emergency fire doors to close
		// triggers alarm to emergency call center 
	
		formula alarmDetected := (heat_detector_factory.alarm = ALARM_STATE.ON &
								 smoke_detector_factory.alarm = ALARM_STATE.ON) |
								 smoke_detector_floor_a.alarm = ALARM_STATE.ON |
								 smoke_detector_floor_b.alarm = ALARM_STATE.ON |
								 smoke_detector_office.alarm = ALARM_STATE.ON;
	
		constant double p_controller_fails := 1e-07;
		constant double p_alarm2doors_fails := 1e-03;
		constant double p_alarm2detectors_fails := 1e-02;
		constant double p_alarm2emergency_fails := 1e-07;
	
		failure controller_fails
			occurs perdemand !alarmDetected probability p_controller_fails;
		endfailure
		
		failure controller_alarm2doors_fails
			occurs perdemand alarmDetected probability p_alarm2doors_fails;
		endfailure 
	
		failure controller_alarm2detectors_fails
			occurs perdemand alarmDetected probability p_alarm2detectors_fails;
		endfailure
		
		failure controller_alarm2emergency_fails
			occurs perdemand alarmDetected probability p_alarm2emergency_fails;
		endfailure	
		
		component fire_alarm_controller_doors
			signal : SIGNAL_STATE init SIGNAL_STATE.UNSET;
			
			signal = SIGNAL_STATE.UNSET & !controller_fails &
			!controller_alarm2doors_fails -> choice:(1:(signal'=SIGNAL_STATE.UNSET));
			signal = SIGNAL_STATE.UNSET & controller_fails &
			!controller_alarm2doors_fails -> choice:(1:(signal'=SIGNAL_STATE.SET));
			signal = SIGNAL_STATE.UNSET & !controller_fails &
			controller_alarm2doors_fails -> choice:(1:(signal'=SIGNAL_STATE.UNSET));
			signal = SIGNAL_STATE.UNSET & controller_fails &
			controller_alarm2doors_fails -> choice:(1:(signal'=SIGNAL_STATE.UNSET)) +
												choice:(1:(signal'=SIGNAL_STATE.SET));
			signal = SIGNAL_STATE.SET -> choice:(1:(signal'=SIGNAL_STATE.SET));
		endcomponent
	
		component fire_alarm_controller_detectors
			signal : SIGNAL_STATE init SIGNAL_STATE.UNSET;
			
			signal = SIGNAL_STATE.UNSET & !controller_fails&
			!controller_alarm2detectors_fails-> choice:(1:(signal'=SIGNAL_STATE.UNSET));
			signal = SIGNAL_STATE.UNSET & controller_fails &
			!controller_alarm2detectors_fails -> choice:(1:(signal'=SIGNAL_STATE.SET));
			signal = SIGNAL_STATE.UNSET & !controller_fails&
			controller_alarm2detectors_fails -> choice:(1:(signal'=SIGNAL_STATE.UNSET));
			signal = SIGNAL_STATE.UNSET & controller_fails &
			controller_alarm2detectors_fails -> choice:(1:(signal'=SIGNAL_STATE.UNSET)) +
													choice:(1:(signal'=SIGNAL_STATE.SET));
			signal = SIGNAL_STATE.SET -> choice:(1:(signal'=SIGNAL_STATE.SET));
		endcomponent
		
		component fire_alarm_controller_emergency
			signal : SIGNAL_STATE init SIGNAL_STATE.UNSET;
			
			signal = SIGNAL_STATE.UNSET & !controller_fails &
			!controller_alarm2emergency_fails -> choice:(1:(signal'=SIGNAL_STATE.UNSET));
			signal = SIGNAL_STATE.UNSET & controller_fails &
			!controller_alarm2emergency_fails -> choice:(1:(signal'=SIGNAL_STATE.SET));
			signal = SIGNAL_STATE.UNSET & !controller_fails &
			controller_alarm2emergency_fails -> choice:(1:(signal'=SIGNAL_STATE.UNSET));
			signal = SIGNAL_STATE.UNSET & controller_fails &
			controller_alarm2emergency_fails -> choice:(1:(signal'=SIGNAL_STATE.UNSET)) +
													choice:(1:(signal'=SIGNAL_STATE.SET));
			signal = SIGNAL_STATE.SET -> choice:(1:(signal'=SIGNAL_STATE.SET));
		endcomponent
	
		/******************************************************************************/
		/* emergency fire doors */
		
		/* failures emergency doors */
		
		formula signalCloseSet := fire_alarm_controller_doors.signal = SIGNAL_STATE.SET;
		
		constant double p_emergency_fire_door_floors_fails := 5e-03;
		constant double p_emergency_fire_door_floor2office_fails := 5e-03;
		
		failure emergency_fire_door_floors_fails
			occurs perdemand  signalCloseSet probability p_emergency_fire_door_floors_fails;
		endfailure
		
		failure emergency_fire_door_floor2office_fails
			occurs perdemand  signalCloseSet probability p_emergency_fire_door_floor2office_fails;
		endfailure
		
		component emergency_fire_door_floors
			state : DOOR_STATE init DOOR_STATE.OPEN;
			
			state = DOOR_STATE.OPEN & !emergency_fire_door_floors_fails -> choice:(1:(state'=DOOR_STATE.CLOSE));
			state = DOOR_STATE.OPEN & emergency_fire_door_floors_fails -> choice:(1:(state'=DOOR_STATE.OPEN));
			state = DOOR_STATE.CLOSE -> choice:(1:(state'=DOOR_STATE.CLOSE));
		endcomponent
		
		component emergency_fire_door_floor2office
			state : DOOR_STATE init DOOR_STATE.OPEN;
			
			state = DOOR_STATE.OPEN & !emergency_fire_door_floor2office_fails -> choice:(1:(state'=DOOR_STATE.CLOSE));
			state = DOOR_STATE.OPEN & emergency_fire_door_floor2office_fails -> choice:(1:(state'=DOOR_STATE.OPEN));
			state = DOOR_STATE.CLOSE -> choice:(1:(state'=DOOR_STATE.CLOSE));
		endcomponent
	 	
		/******************************************************************************/
	endcomponent

endcomponent